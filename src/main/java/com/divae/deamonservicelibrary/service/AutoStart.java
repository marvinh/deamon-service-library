package com.divae.deamonservicelibrary.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.divae.deamonservicelibrary.service.storage.database.ServiceDatabaseStorageDriver;

import java.math.BigInteger;
import java.util.Map;

/**
 * This class is for handling BOOT_COMPLETED system messages, which are sent by the android system
 * when the devices has finished boot. If such a message has been received, the onReceive method is called.
 */

public class AutoStart extends BroadcastReceiver {

    /**
     * This method is called when a BOOT_COMPLETE system message has been received. It tries to start
     * the daemon service, when it ran before the system has been shutdown and returns.
     * */
    @Override
    public void onReceive(Context context, Intent intent) {
        ServiceDatabaseStorageDriver driver = new ServiceDatabaseStorageDriver(context);
        Map<BigInteger, ServiceInfo> serviceInfoMap = driver.loadObjects();

        for (ServiceInfo serviceInfo : serviceInfoMap.values()){
            if (serviceInfo.getState() == ServiceInfo.State.STARTONBOOT){
                DeamonService.startService(
                        context,
                        serviceInfo.getStartupDelay(),
                        serviceInfo.getInterval(),
                        serviceInfo.getPeriodicAction().getAction());
            }
        }

    }
}
