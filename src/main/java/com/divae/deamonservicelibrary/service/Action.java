package com.divae.deamonservicelibrary.service;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * This interface is being implemented for services which shall run some action
 * periodically.
 */

public interface Action extends Serializable{
    /**
     * This method contains the action to be done periodically by the DaemonService
     * */
    void perform(ServiceBinder serviceBinder, String tag);

    /**
     * This method returns the unique Service ID for this kind of action. It has to
     * be unique for all actions that shall be executed as a Service
     * @return ID of the Service related to this action
     */
    BigInteger getID();
}
